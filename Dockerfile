FROM tomcat:7-jre7-alpine

COPY ./webapp.war /usr/local/tomcat/webapps/webapp.war

WORKDIR /usr/local/tomcat/webapps/

EXPOSE 80

